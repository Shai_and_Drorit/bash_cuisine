# Appetizer of the day : tiny shell kebab

- create shell script  named `food_shopping.sh`, that has several functions:
    - get meat function
    - get spices 
    - mix it all 
    - cook it in oven
    - cook it on flat fire
    - grid meat and spices
    - wrap in pita
all functions should have validation if the ingridients are ready

- create  shell script called `at_the_kitchen.sh` that imports all functions, checks that they are all correct/true/valid and has some functions of its own:
    - start the oven function
    - start the flat gas function

- last but not least: 
    create a script called `lets_feast.sh`, that uses previous 2 scripts and its own functions `getting the table ready` and `getting drinks` and at the end ofusing all scripts and functions
    wishes you `bon appetit`


#### NOTE:
 please use variables, and bash variable substitution for all the ingridients, as well as function return values.
 external tool that can be useful:
 - https://devhints.io/bash
 - figlet --> apt/yum install -y figlet
 