#!/usr/bin/env bash

#    Owner: Shai
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Date: 
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Version: 1.0.0
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#    Purpose: 
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

source food_shopping.sh
function_exists() {
    declare -f -F $1 > /dev/null
    return $?
}

function_exists oven_cook
if [ "$?" -eq "0" ];then
 oven_cook "Keves" 
else
 echo "failed" && logger "$0 failed oven_cook function"
fi
