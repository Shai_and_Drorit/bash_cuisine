#!/usr/bin/env bash
############################
# owner: droritzz
# purpose: learn bash
# date: 20.3.21
# vesrion: v1.0.0
###########################

create_omelet(){
	eggs="O O O O"
	spices=(salt pepper)
	shortening="butter"
	cook=""
	echo "warming the pan"
	sleep 2
	echo "adding the shortening"
	cook+="${shortening} "
	sleep 3
	echo "let's break some eggs"
	cook+="${eggs} "
	echo "don't forget to season!"
	for spice in "${spices[@]}"
	do
		cook+="${spice} "
	done
	sleep 3
	echo "eat while it's hot!"
}
count=1
while [[ $count -le 2 ]]
do
	echo "making omelet $count"
	create_omelet
	let count++
done
echo "you've cooked 2 omelets"


