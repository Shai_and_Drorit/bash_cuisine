#!/usr/bin/env bash
############################
# owner: droritzz
# purpose: learn bash
# date: 20.3.21
# vesrion: v1.0.0
###########################

create_omelet(){
	some_extra_for_you=$1
	eggs="O O O O"
	spices=(salt pepper)
	shortening="butter"
	extras=(onion garlic cheese)
	cook=""
	echo "warming the pan"
	sleep 2
	echo "adding the shortening"
	cook+="${shortening} "
	sleep 3
	echo "you sure want some extra only for you, let's add it!"
	cook+="${some_extra_for_you} "
	echo "let's break some eggs"
	cook+="${eggs} "
	echo "some extras, because you deserve it"
	for extra in "${extras[@]} "
	do
		cook+="${extra} "
	done
	sleep 2
	echo "don't forget to season!"
	for spice in "${spices[@]}"
	do
		cook+="${spice} "
	done
	sleep 3
	echo $cook
	echo "eat while it's hot!"
}
mix_n_cook(){
	echo "grab a plate"
	create_omelet bacon 
}
mix_n_cook

