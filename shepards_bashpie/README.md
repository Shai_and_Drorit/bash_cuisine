# Shepards Bash Pie

We'll preparing Shepards bash pie tonight.
but to make it work we'll have to create a lot of small things and then combine them together:

- Heat it up the oven and check it every 3 mins if it reached 180 degrees celcius (cron job that runs a script that increases custom system variable OVEN_HEAT every 3 mins and stops when the value is equal to 180. the value should increase exponentially. you need to validate that cron is running.)
  
- Keep list of required ingridients for shepards bash pie (create a script that requires you to enter values to array that is system variable. if the values are not correct, error should be provided ... yes you need to pre setup ingridients with script before hand)
  
- Setup storage for your ingridients (script that creates partition on external device called `fridge` where you keep your ingridients sorted alphabetically in a file called `shepards_bash_pie.array`, after you have validated that they are in the array mentioned before-hand)

- Gring the ingridients and mix them up.(take every ingridients and create array of characters of each ingridient and then mix them, e.g   cheese, meat --> c h e e s e m e a t --> cemaetehse)

- cook it all. (set a cron job that uses script which purpose is to log the temperature of the oven and value of mixed ingridient list with time and date. )

- Once done, use tool named `figlet` to print the  `Shepards Bash Pie` mixed ingridients and to wish the users name(Yes... you need to ask him/her for the name or pre-define the vairable.) `bon appette`.