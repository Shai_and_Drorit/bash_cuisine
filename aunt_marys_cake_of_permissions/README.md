# Aunt Mary's Specialty : Cake of Permissions

## Aunt Mary is known for her temper, yet she makes great cakes. 
To help and not to make her mad we'll have to behave and be usefull at the same time.
Let start by getting her ingredients:

- Cake ingredient 1: wrtie a function that takes and/or requests UID from you and if you are in cake makers group(sudo/wheel), it "yells" at you and then removes you from the kitchen. if you are in the group, then it will let you know that you are welcome in the kitchen.
  
- Cake ingredient 2: write a function that will save list of nesecities for cakes and once you say you are done, it will list them one by one, with timeout interval of 1.5 seconds. if any of necesities are repeted then it needs to be removed from the list.
  
- Cake ingredient 3: next function will have to clean the working area in kitchen and save all the ingredients with permissions  to cake makers groups.(verify that you have storage area(disk, lvm, folder or alike) for"cake maker" group and only the group should be operable there")

