#!/usr/bin/env bash
###############################
#owner: droritzz
#purpose: manage group permissions: bash cuisine - feed the Justice League - add users
#date: 20/03/21
#version: in git commit
##############################

#- setup all the users with your script and give them all excrypted password that is different from each other.
#- setup groups according to the names set in description
#- manage groups with set-group-id (folders need to have group permission)
#########################################
# relax dir - rwx
# prepare dir = prepare+relax dir rwx
# set up dir= group+prepare+relax rwx

#realx - Selina, Robert, Dinah, Zatanna
#prepare - Louis, Oliver, Clark, Ray
#main - J'onn, Shiera, Bruce, me

#set up all users and passwords
#set up groups for the users with permissions
#set up dining area - storage on external disk --> mkfs it --> mount it. test if it accessable from /home
#########################################



create_groups(){
    echo "creating groups"
    groups=("relax" "prepare" "main")
    for group in "${groups[@]}" 
    do
    groupadd $group 
    done
    #exitstatus
}
create_users(){
    relax_group=("Selina" "Robert" "Dinah" "Zatanna")
    prepare_group=("Louis" "Oliver" "Clark" "Ray")
    main_group=("John" "Shiera" "Bruce")
    
    for relax in "${relax_group[@]}" 
    do
    useradd -G "${groups[0]}" "${relax}" 
    #read -s -p "please enter password for the $relax : " password
    #echo -e "$password\n$password" | passwd $relax
    #exitstatus
    done

    for prepare in "${prepare_group[@]}" 
    do
    useradd -G "${groups[1]}" "${prepare}"
    #read -s -p "please enter password for the "${prepare}" : " password
    #echo -e "$password\n$password" | passwd "${prepare}"
    #exitstatus 
    done

    for main in "${main_group[@]}" 
    do
    useradd -G "${groups[2]}" ${main}
    #read -s -p "please enter password for the $main: " password
    #echo -e "$password\n$password" | passwd $main
    #exitstatus
    done
}
create_groups
create_users






