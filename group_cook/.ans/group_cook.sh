#!/usr/bin/env bash
###############################
#owner: droritzz
#purpose: manage group permissions: bash cuisine - feed the Justice League
#date: 20/11/20
#version: in git commit
##############################

## handle group managed permissions
# As part of short field trip you, Ray, Shiera, Zatanna, Robert, J'onn, Bruce, Selina, Dinah, Oliver, Clark and Louis need a script hat can help you manage user and group permission so:
#- those who wish to relax, need to have access, write and execute permissions to `relax` folder at the dining area (storage folder on external drive,usb or disk)
#- those who wish to help out, can can have access, write and execute permissions to `prepare` area and to `relax` area as well.
#- those who wish to setup the whole thing, can access to whole group in whole area as well as `prepare` and `relax` areas well.
#- make sure that areas are available before hand, and that they are accessable from main system(`/home`)

###### NOTE:
#- use fdisk and mkfs to setup the exeternal disk/usb and there you should have `main` area and in it create `relax` and `prepare`
#- setup all the users with your script and give them all excrypted password that is different from each other.
#- setup groups according to the names set in description
#- manage groups with set-group-id (folders need to have group permission)


##########################################
# relax dir - rwx
# prepare dir = prepare+relax dir rwx
# set up dir= group+prepare+relax rwx

#realx - Selina, Robert, Dinah, Zatanna
#prepare - Louis, Oliver, Clark, Ray
#main - J'onn, Shiera, Bruce, me

#set up all users and passwords
#set up groups for the users with permissions
#set up dining area - storage on external disk --> mkfs it --> mount it. test if it accessable from /home
#########################################

###################### PERMISSIONS #################################
# dear Albert, you'll have to manage this mess. Be sure you're ROOT
###################################################################

################ FUNCTIONS ###########################

exitstatus(){
	if [[ $? -gt 0 ]]; then
		echo "cannot complete the task"
		exit 1
	else 
		echo "done"
	fi
}


validation(){
	if [[ -z $@ ]]; then
		echo "please provide data"
		exit 1
	fi
}

create_groups(){
    echo "creating groups"
    groups=("relax" "prepare" "main")
    for group in "${groups[@]}" 
    do
    groupadd $group 
    done
    #exitstatus
}
create_users(){
    relax_group=("Selina" "Robert" "Dinah" "Zatanna")
    prepare_group=("Louis" "Oliver" "Clark" "Ray")
    main_group=("John" "Shiera" "Bruce")
    
    for relax in "${relax_group[@]}" 
    do
    useradd -G "${groups[0]}" "${relax}" 
    #read -s -p "please enter password for the $relax : " password
    #echo -e "$password\n$password" | passwd $relax
    #exitstatus
    done

    for prepare in "${prepare_group[@]}" 
    do
    useradd -G "${groups[1]}" "${prepare}"
    #read -s -p "please enter password for the "${prepare}" : " password
    #echo -e "$password\n$password" | passwd "${prepare}"
    #exitstatus 
    done

    for main in "${main_group[@]}" 
    do
    useradd -G "${groups[2]}" ${main}
    #read -s -p "please enter password for the $main: " password
    #echo -e "$password\n$password" | passwd $main
    #exitstatus
    done
}

check_amount_of_partitions(){
	amount_of_partitions=$(fdisk -l /dev/$set_up | grep -A100 "Device" |wc -l)
echo "you have $amount_of_partitions partitions on this disk"
}       

delete_partitions(){
fdisk /dev/$set_up <<- EOL
	d

	w
	EOL
}

create_partition(){
	local size_of_set_up=$1
	read -p "please provide the size of the set up " size_of_set_up
	fdisk /dev/$set_up <<- EOL
	n
	p

	+$size_of_set_up
	w
	EOL
}

access_to_set_up(){
	set_up_partition=$(fdisk -l $set_up |awk '{print $1}'|grep '/dev')
	mke2fs $set_up_partition
	mkdir -p /mnt/$set_up_partition
	mount /dev/$set_up /mnt/$set_up_partition
}


######################################################

echo "Welcome, Alfred. you have to manage this trip. be sure you have permissions."
set_up=""
read -p "please provide disk for the set up " set_up
echo "making sure you can access the disk, it will take a few seconds"
mount | grep /dev/$set_up

if [[ $? -eq 0 ]]; then
	echo "the disk is mounted. what do you wish to do? "
	printf \n "please select action "
	select option in formate_disk procede
		case $option in 
			"formate_disk" )
				check_amount_of_partitions
				count=0
				
#	while [[ $amount_of_partitions -gt $count ]];
#	do
#		echo "making sure the set up area is empty"
#		delete_partitions 
#		let count++
	#	exitstatus
#		echo "creating the area for the set up"
#		create_partition
	#	exitstatus
#	done
#else
#	echo "the path is ready. who is coming"
#fi


#	cd /SET_UP --> create main dir --> create relax and prepare dir 




	


