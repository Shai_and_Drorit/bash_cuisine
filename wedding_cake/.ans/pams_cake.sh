#!/usr/bin/env bash

###############################
#owner: mondbev and droritzz
#purpose: learning lvm
#date: 25.11.20
#version in git commit
###############################

############### FUNCTIONS #####################
decoration(){
_time=2.5
l="-----------------------------------------"
clear
printf "$l\n# %s\n$l" "$@"
sleep $_time
clear
}

############## VARIABLES ######################
_disk=/dev/sda
disk_num=("1" "3" "4")
_log=" $0 error log at: "
##############################################

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root. Exit"
	logger $_log "No permission"
   exit 1
fi

decoration "Creating partitions to /dev/sda 1,3,4"

count=0
disk_num_size=$(( ${#disk_num[@]} -1 ))
while [ $count -le $disk_num_size ];do
	dev="${disk_num[$count]}"
	dev_name+=("$_disk$dev")
fdisk $_disk << EOWORLD
t
$dev
8e
w
EOWORLD
mkfs.ext4 ${dev_name[$count]}
	let count++
done
lvmdiskscan
sleep 2

decoration "Creating PV"

pvcreate ${dev_name[*]}
pvs
sleep 2

decoration "Creating VG"

vgcreate cake ${dev_name[*]}
vgs
sleep 2
decoration "Creating striped volume"

lvcreate -L 1G -I 4096 -i 3 -n cake_vol cake
lvs
sleep 2

decoration "Adding cream Changing to pams_cake"

vgrename cake pams_cake
vgs
sleep 2

decoration "Extending cake size"

vgreduce pams_cake /dev/sda1
pvremove /dev/sda1
pvcreate /dev/sda1
vgextend pems_cake /dev/sda1

decoration "Slicing 2 equal slices"

lvcreate -l 50%VG -n slice1 pams_cake
lvcreate -l 50%VG -n slice2 pams_cake
vgs
sleep 3
exit 0
