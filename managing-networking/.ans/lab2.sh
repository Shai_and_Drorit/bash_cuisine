#!/usr/bin/env bash


net_class(){

a=$(seq 128 191 | sort -R | head -n 1)
b=$(seq 1 254 | sort -R | head -n 1)
c=$(seq 1 254 | sort -R | head -n 1)
d=$(seq 1 254 | sort -R | head -n 1)
echo "$2 in class $1 optimal IP: $a.$b.$c.$d "
}
ip_conf(){
local interface="$1"
echo "Let's create a network for $interface"
echo ****
echo "Class A   0.0.0.0 - 127.255.255.255 /8";
echo **
echo "Class B 128.0.0.0 - 191.255.255.255 /16";
echo **
echo "Class C 192.0.0.0 - 239.255.255.255 /24"
echo **** 
echo "Please select class"
select i in a b c 
do
case $i in
	a) echo "A" net_class "A" "$1";;
	b) echo "B" net_class "B" "$1";;
	c) echo "C" net_class "C" "$1";;
	*) echo "Give input" break;;
esac
done
}
# Print out all interfaces on computer
ip addr show

# Get interfaces 

sleep 2
cols=($(strings <<<$(ip addr | awk -F': ' '{print $2}')))
interfaces=($(tr ' ' '\n' <<< "${cols[@]}" | sort -u | tr '\n' ' '))

# Check if intefaces have IP address

for inter in "${interfaces[@]}";do
temp=$(ip -4 addr show dev "$inter" | grep inet | tr -s " " | cut -d" " -f3 | head -n 1 )
[ -n "$temp" ] > /dev/null 2>&1 || ip_conf "$inter"
done
